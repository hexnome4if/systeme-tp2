#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <errno.h>
#include <string.h>

int main(){
	size_t size = 0x40000000;
	int page_size = getpagesize();
	char* ptr;

	printf("Page size: %d\n", page_size);

	for(int i=1; i<=1024; i++){
		ptr = (char*) malloc(size);
		printf("malloc number %d -->", i);
		
		if(ptr != NULL){
			*(ptr+3) = 'a';
			printf(" allocated at %p \n", ptr);
		} else {
			perror(strerror(errno));
		}
	}
	return 0;
}
